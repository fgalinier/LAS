note
	explicit: "all"

class
	LAS

-- State ranges
feature {NONE}
	-- Allocation state range
	allocate: INTEGER = 0
	deallocate: INTEGER = 1

	-- Availability state range
	free: INTEGER = 2
	assigned: INTEGER = 3

	-- Mobilized state range
	mobilize: INTEGER = 4
	demobilize: INTEGER = 5

	-- Intervention state range
	intervene: INTEGER = 6
	withdraw: INTEGER = 7

	-- Encoded state range
	encode: INTEGER = 8
	decode: INTEGER = 9

	-- State space
	allocated: INTEGER
	available: INTEGER
	mobilized: INTEGER
	intervention: INTEGER
	encoded: INTEGER

	distance: INTEGER

-- Requirements
feature

	-- Assume the system is
	run_in_normal_condition
	do
		-- allocated status range:
		check assume: allocated = allocate or allocated = deallocate end
		-- available status range:
		check assume: available = free or available = assigned end
		-- mobilized status range:
		check assume: mobilized = mobilize or mobilized = demobilize end
		-- intervention status range:
		check assume: intervention = intervene or intervention = withdraw end
		-- encoded status range:
		check assume: encoded = encode or encoded = decode end
		main
	end

	---------------

	-- Once an incident is encoded, an
	-- available ambulance must be allocated within the next two
	-- time units

	-- Require an ambulance to be
	allocated_within_two_time_units
	local
		old_distance: INTEGER
	do
		from
        	old_distance := distance
			occurs_encode
      	until
			allocated = allocate or
	        (distance - old_distance) >= 2
      	loop
        	allocate_ambulance
		end
		check assert: allocated = allocate end
		check assert: distance - old_distance <= 2 end
	end

	-- Assume
	occurs_encode
	do
		check assume: encoded = encode end
		run_in_normal_condition
	end

	-- Assume
	from_not_allocated_to_allocated
	local
		old_allocated: INTEGER
	do
		old_allocated := allocated
		allocate_ambulance
		if old_allocated /= allocate and allocated = allocate then
			distance := distance + 1
		end
	end

	---------------

	-- an ambulance is mobilized within the first two time units 
	-- after an ambulance has been allocated to the incident site.

	-- Require an ambulance to be
	mobilized_within_two_time_units
	local
		old_distance: INTEGER
	do
		from
        	old_distance := distance
			occurs_allocate
      	until
			mobilized = mobilize or
	        (distance - old_distance) >= 2
      	loop
        	mobilize_ambulance
		end
		check assert: mobilized = mobilize end
		check assert: distance - old_distance <= 2 end
	end

	-- Assume
	occurs_allocate
	do
		check assume: allocated = allocate end
		run_in_normal_condition
	end

	-- Assume
	from_not_mobilized_to_mobilized
	local
		old_mobilized: INTEGER
	do
		old_mobilized := mobilize
		mobilize_ambulance
		if old_mobilized = demobilize and mobilized = mobilize then
			distance := distance + 1
		end
	end
	
	---------------

	-- Require an ambulance that is
	allocated_is_not_available
	do
		run_in_normal_condition
		check assert: allocated = allocate implies available /= free end
		check assert: available /= free implies allocated = allocate end
	end

	---------------

	-- Require an ambulance to be
	not_mobilized_if_not_allocated
	do
		ambulance_not_allocated
		check assert: mobilized /= mobilize end
	end

	-- Assume that
	ambulance_not_allocated
	do
		check assume: allocated /= allocate end
		run_in_normal_condition
	end

	---------------

	-- Require an ambulance to be
	not_demobilized_if_allocated_and_not_intervened
	do
		ambulance_allocated_and_not_intervened
		check assert: mobilized /= demobilize end
	end

	-- Assume that
	ambulance_allocated_and_not_intervened
	do
		check assume: allocated = allocate end
		check assume: intervention /= intervene end
		occurs_encode
	end

feature {NONE}
	
	---------------
	-- Behaviour of the system
	---------------

	-- Assuming that an incident is encoded when the incident is signaled
	-- and decoded when the incident is signaled as being close.
	main
	do
		if encoded = encode then
			if allocated = deallocate then
				allocate_ambulance
			end
			if available = free then
				assign_ambulance
			end
			if mobilized = demobilize then
				mobilize_ambulance
			end
		elseif encoded = decode then
			if intervention = intervene then
				withdraw_ambulance
			end
			if mobilized = mobilize then
				demobilize_ambulance
			end
			if available = assigned then
				free_ambulance
			end
			if allocated = allocate then
				deallocate_ambulance
			end
		end
	end	

	mobilize_ambulance
	require
		not_mobilized: mobilized /= mobilize
		not_intervened: intervention /= intervene
	do
		mobilized := mobilize
		intervention := intervene
	end

	demobilize_ambulance
	require
		not_demobilized: mobilized /= demobilize
	do
		mobilized := demobilize
	end
	
	assign_ambulance
	require
		not_assigned: available /= assigned
	do
		available := assigned
	end

	free_ambulance
	require
		not_free: available /= free
	do
		available := free
	end
	
	allocate_ambulance
	require
		not_allocated: allocated /= allocate
	do
		allocated := allocate
	end

	deallocate_ambulance
	require
		not_deallocated: allocated /= deallocate
	do
		allocated := deallocate
	end

	withdraw_ambulance
	require
		not_withdrawed: intervention /= withdraw
	do
		intervention := withdraw
	end

	intervene_ambulance
	require
		not_intervened: intervention /= intervene
	do
		intervention := intervene
	end
end
